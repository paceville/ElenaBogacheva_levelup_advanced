package ru.levelup.at.advance.taf.api.pojo.createbooking;

import com.github.javafaker.Faker;

public record CreateBooking(
        String firstname,
        String lastname,
        Integer totalprice,
        Boolean depositpaid,
        Bookingdates bookingdates,
        String additionalneeds
) {

    public static String getFirstName() {
        return new Faker().name().firstName();
    }

    public static String getLastName() {
        return new Faker().name().lastName();
    }

    public static int getPrice(int a, int b) {
        return (a + (int) (Math.random() * b));
    }

    public static String getAdditionalInfo() {
        return new Faker().howIMetYourMother().catchPhrase();
    }


}
