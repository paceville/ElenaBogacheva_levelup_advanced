package ru.levelup.at.advance.taf.ui.service.page.object;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;

public class BankManagerPage {
    public SelenideElement addCustomerButton = $(byAttribute("ng-click^", "addCust"));
    public SelenideElement submitButton = $(byAttribute("type", "submit"));
    public SelenideElement openAccount = $(byAttribute("ng-click^", "openAccount"));

    //FIELDS
    private final SelenideElement addCustomerForm = $(byAttribute("ng-submit^", "addCustomer"));
    public SelenideElement firstName = addCustomerForm.$(byAttribute("ng-model", "fName"));
    public SelenideElement lastName = addCustomerForm.$(byAttribute("ng-model", "lName"));
    public SelenideElement postCode = addCustomerForm.$(byAttribute("ng-model", "postCd"));
    public SelenideElement userSelect = $(byId("userSelect"));
    public SelenideElement currency = $(byId("currency"));
}
