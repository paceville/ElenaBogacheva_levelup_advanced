package ru.levelup.at.advance.taf.api.pojo.createbooking;

import java.time.LocalDate;

public record Bookingdates(
    LocalDate checkin,
    LocalDate checkout
) {

    public static LocalDate getNowPlusDays(int countOfDaysAfterNow) {
        return LocalDate.now().plusDays(countOfDaysAfterNow);
    }

    public static LocalDate getNowMinusDays(int countOfDaysBeforeNow) {
        return LocalDate.now().minusDays(countOfDaysBeforeNow);
    }

}
