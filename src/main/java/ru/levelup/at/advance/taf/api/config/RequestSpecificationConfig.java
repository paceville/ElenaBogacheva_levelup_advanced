package ru.levelup.at.advance.taf.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import java.io.File;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class RequestSpecificationConfig {

    @SneakyThrows
    public static RequestSpecification defaultSpecification() {
        final var configFilePath = System.getenv("CONFIG_PATH");

        if (configFilePath == null) {
            throw new IllegalArgumentException("Please set up CONFIG_PATH variable to env variables");
        }

        var apiConfiguration = new ObjectMapper().readValue(new File(configFilePath), ApiConfiguration.class);

        return new RequestSpecBuilder()
                .setBaseUri(apiConfiguration.url())
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }
}
