package ru.levelup.at.advance.taf.ui.service.page.object;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;

public class LoginPage {
    public SelenideElement customerLoginButton = $(byAttribute("ng-click^", "customer"));
    public SelenideElement bankManagerLoginButton = $(byAttribute("ng-click^", "manager"));
    public SelenideElement homeButton = $(byAttribute("class$", "home"));

}
