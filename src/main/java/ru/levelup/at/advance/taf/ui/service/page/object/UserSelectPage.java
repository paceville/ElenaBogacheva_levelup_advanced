package ru.levelup.at.advance.taf.ui.service.page.object;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class UserSelectPage {
    public SelenideElement userSelectDropDownList = Selenide.$x("//*[@id='userSelect']");
    public SelenideElement loginButton = Selenide.$x("//*[@class='btn btn-default']");
}
