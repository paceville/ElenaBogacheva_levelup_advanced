package ru.levelup.at.advance.taf.ui.service.page.object;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;

public class UserAccountPage {
    public SelenideElement accountSelectButton = $(byId("accountSelect"));
    public SelenideElement deposit = $(byAttribute("ng-click^", "deposit"));
    public SelenideElement amountToBeDeposited = $(byAttribute("ng-model^", "amount"));
    public SelenideElement accountNumber = $(withText("Account Number"));
    public SelenideElement transactionsButton = $(byAttribute("ng-click^", "transactions"));
    public SelenideElement depositSuccessfulMessage = $(byAttribute("ng-show", "message"));
    public SelenideElement withdrawButton = $(byAttribute("ng-click^", "withdrawl"));
    public SelenideElement amountToBeWithdrawn = $(byAttribute("type", "number"));
    public SelenideElement submitButton = $(byAttribute("type", "submit"));
    public SelenideElement submitWithdrawalRequestErrorMessage = $(byClassName("error"));

    public String getBalance() {
        return accountNumber.getText().split(" , ")[1].split(" : ")[1];
    }

    public String getCurrency() {
        return accountNumber.getText().split(" , ")[2].split(" : ")[1];
    }

}

