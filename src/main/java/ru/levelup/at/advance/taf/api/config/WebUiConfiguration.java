package ru.levelup.at.advance.taf.api.config;

public record WebUiConfiguration(
        String url,
        Boolean headless
) {
}
