package ru.levelup.at.advance.taf.api.client;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.RequiredArgsConstructor;
import ru.levelup.at.advance.taf.api.config.SerializationAndDeserialization;

@RequiredArgsConstructor(staticName = "of")
public class BookingApiClient {
    private static final String BOOKING_ENDPOINT = "/booking";
    private static final String BOOKING_ENDPOINT_ID = BOOKING_ENDPOINT + "/{bookingid}"; //this is path parameter

    private final RequestSpecification requestSpecification;

    public Response createBooking(Object bodyObject) {

        String requestBody = SerializationAndDeserialization.serialization(bodyObject);

        return given()
                .spec(requestSpecification)
                .body(requestBody)
                .when()
                .post(BOOKING_ENDPOINT)
                .andReturn();
    }

    public Response getBooking(final String bookingid) {
        return given()
                .spec(requestSpecification)
                .pathParam("bookingid", bookingid)
                .when()
                .get(BOOKING_ENDPOINT_ID)
                .andReturn();
    }

}
