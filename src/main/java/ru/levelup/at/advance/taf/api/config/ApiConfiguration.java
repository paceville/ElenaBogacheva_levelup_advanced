package ru.levelup.at.advance.taf.api.config;

public record ApiConfiguration(
        String url
) {
}
