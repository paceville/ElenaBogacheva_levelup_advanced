package ru.levelup.at.advance.taf.ui.service.helpers;

import com.github.javafaker.Faker;

public class TestData {
    public static final String CUSTOMER = "customer";
    public static final String BANK_MANAGER = "bankManager";
    public static final String RUPEE = "Rupee";
    public static final String DOLLAR = "Dollar";
    public static final String POUND = "Pound";
    public static final String ERROR_MESSAGE = "Transaction Failed. You can not withdraw amount more than the balance.";
    public static final String CUSTOMER_ADDED_MESSAGE = "Customer added successfully with customer id";

    public static String getFirstName() {
        return new Faker().name().firstName();
    }

    public static String getLastName() {
        return new Faker().name().lastName();
    }

    public static String getPostcode() {
        return String.valueOf(10000 + (int) (Math.random() * 99999));
    }
}
