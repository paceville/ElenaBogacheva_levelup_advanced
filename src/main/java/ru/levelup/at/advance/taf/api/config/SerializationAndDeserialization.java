package ru.levelup.at.advance.taf.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.StringWriter;
import lombok.SneakyThrows;

public class SerializationAndDeserialization {

    @SneakyThrows
    public static String serialization(Object bodyObject) {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.writeValue(writer, bodyObject);

        return writer.toString();
    }
}
