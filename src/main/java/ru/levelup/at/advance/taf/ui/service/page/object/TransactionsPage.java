package ru.levelup.at.advance.taf.ui.service.page.object;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;

public class TransactionsPage {
    public SelenideElement resetButton = $(byAttribute("ng-click^", "reset"));
    public SelenideElement backButton = $(byAttribute("ng-click^", "back"));
    private final SelenideElement row = $(byId("anchor0"));

    public String getDateTimeOfTransaction() {
        return row.$$("td").get(0).getText().trim();
    }

    public String getAmountOfMoney() {
        return row.$$("td").get(1).getText().trim();
    }

    public Boolean isTableNotEmpty() {
        return row.isDisplayed();
    }
}
