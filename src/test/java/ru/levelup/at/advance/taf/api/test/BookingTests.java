package ru.levelup.at.advance.taf.api.test;

import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.Assertions.assertThat;
import static ru.levelup.at.advance.taf.api.pojo.createbooking.Bookingdates.getNowMinusDays;
import static ru.levelup.at.advance.taf.api.pojo.createbooking.Bookingdates.getNowPlusDays;
import static ru.levelup.at.advance.taf.api.pojo.createbooking.CreateBooking.getAdditionalInfo;
import static ru.levelup.at.advance.taf.api.pojo.createbooking.CreateBooking.getFirstName;
import static ru.levelup.at.advance.taf.api.pojo.createbooking.CreateBooking.getLastName;
import static ru.levelup.at.advance.taf.api.pojo.createbooking.CreateBooking.getPrice;

import io.qameta.allure.Attachment;
import java.util.stream.Stream;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.levelup.at.advance.taf.api.client.BookingApiClient;
import ru.levelup.at.advance.taf.api.config.RequestSpecificationConfig;
import ru.levelup.at.advance.taf.api.pojo.createbooking.Bookingdates;
import ru.levelup.at.advance.taf.api.pojo.createbooking.CreateBooking;

class BookingTests {

    @ParameterizedTest()
    @DisplayName("Create a new happy path booking with parameters: ")
    @MethodSource("provideArgumentsHappyPath")
    @Attachment
    void testCreateBookingPositive(CreateBooking newBooking) {

        final var response = step("Create a new booking",
                () -> BookingApiClient.of(RequestSpecificationConfig.defaultSpecification())
                .createBooking(newBooking))
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK);

        final String actualBookingId = response.extract().body().jsonPath().get("bookingid").toString();
        final var actualBookingResponse = BookingApiClient.of(RequestSpecificationConfig.defaultSpecification())
                .getBooking(actualBookingId)
                .then()
                .log().all();

        final String firstname = actualBookingResponse.extract().body().jsonPath().get("firstname");
        assertThat(firstname)
                .as("Check that it's our booking")
                .isEqualTo(newBooking.firstname());
    }

    @ParameterizedTest()
    @DisplayName("Create a new booking with null values: ")
    @MethodSource("provideArgumentsNullTests")
    @Attachment
    void testCreateBookingWithNullValues(CreateBooking newBooking) {

        step("Create a new booking",
            () -> BookingApiClient.of(RequestSpecificationConfig.defaultSpecification())
                                  .createBooking(newBooking))
            .then()
            .log().all()
            .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @ParameterizedTest()
    @DisplayName("Create a new booking: negative tests with parameters: ")
    @MethodSource("provideArgumentsNegativeTests")
    @Attachment
    void testCreateBookingNegative(CreateBooking newBooking) {

        step("Create a new booking",
            () -> BookingApiClient.of(RequestSpecificationConfig.defaultSpecification())
                                  .createBooking(newBooking))
            .then()
            .log().all()
            .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    private static Stream<Arguments> provideArgumentsHappyPath() {
        return Stream.of(
            Arguments.of(new CreateBooking(getFirstName(), getLastName(), getPrice(100, 10000), true,
                new Bookingdates(getNowPlusDays(30), getNowPlusDays(45)), getAdditionalInfo()))
        );
    }


    static Stream<? extends Arguments> provideArgumentsNullTests() {
        return Stream.of(
            Arguments.of(new CreateBooking(null, null, null, null,
                new Bookingdates(null, null), null)),

            Arguments.of(new CreateBooking(null, getLastName(), getPrice(100, 10000), true,
                new Bookingdates(getNowPlusDays(100),
                    getNowPlusDays(105)), getAdditionalInfo()))
        );
    }

    static Stream<? extends Arguments> provideArgumentsNegativeTests() {
        return Stream.of(
            //checkin < now
            Arguments.of(new CreateBooking(getFirstName(), getLastName(), getPrice(100, 10000), true,
                new Bookingdates(getNowMinusDays(10),
                    getNowPlusDays(15)), getAdditionalInfo())),

            //checkout < now
            Arguments.of(new CreateBooking(getFirstName(), getLastName(), 1000, true,
                new Bookingdates(getNowMinusDays(10),
                    getNowMinusDays(15)), getAdditionalInfo()))

        );
    }
}
