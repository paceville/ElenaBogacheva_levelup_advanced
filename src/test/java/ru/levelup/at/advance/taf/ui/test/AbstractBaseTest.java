package ru.levelup.at.advance.taf.ui.test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import ru.levelup.at.advance.taf.api.config.WebUiConfiguration;
import ru.levelup.at.advance.taf.api.config.WebUiConfigurationProvider;

public abstract class AbstractBaseTest {
    protected WebUiConfiguration config;

    @Step("Open browser")
    @BeforeEach
    void setUp() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        config = WebUiConfigurationProvider.getConfig();
        Configuration.headless = config.headless();
        Selenide.open(config.url());
    }

    @Step("Close browser")
    @AfterEach
    void tearDown() {
        Selenide.closeWebDriver();
    }
}
