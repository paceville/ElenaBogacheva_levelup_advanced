package ru.levelup.at.advance.taf.ui.test;

import static com.codeborne.selenide.Selenide.switchTo;
import static java.lang.Integer.parseInt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.BANK_MANAGER;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.CUSTOMER;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.CUSTOMER_ADDED_MESSAGE;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.DOLLAR;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.ERROR_MESSAGE;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.POUND;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.RUPEE;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.getFirstName;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.getLastName;
import static ru.levelup.at.advance.taf.ui.service.helpers.TestData.getPostcode;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import ru.levelup.at.advance.taf.ui.service.page.object.BankManagerPage;
import ru.levelup.at.advance.taf.ui.service.page.object.LoginPage;
import ru.levelup.at.advance.taf.ui.service.page.object.TransactionsPage;
import ru.levelup.at.advance.taf.ui.service.page.object.UserAccountPage;
import ru.levelup.at.advance.taf.ui.service.page.object.UserSelectPage;

public class BankTests extends AbstractBaseTest {
    LoginPage loginPage = new LoginPage();
    UserSelectPage userSelectPage = new UserSelectPage();
    UserAccountPage userAccountPage = new UserAccountPage();
    BankManagerPage bankManagerPage = new BankManagerPage();
    TransactionsPage transactionsPage = new TransactionsPage();


    @Test
    @DisplayName("Top up")
    public void testTopUp() {
        userLogin(CUSTOMER);
        chooseRandomCustomer();
        findAccountWithCurrency(RUPEE);
        resetTransactionsHistory();
        int amountOfMoney = topUp();
        checkBalance(amountOfMoney);
        checkTransaction(amountOfMoney);
    }

    @Test
    @DisplayName("Open account for new customer")
    public void testOpenAccountForNewCustomer() {
        userLogin(BANK_MANAGER);
        var customer = createNewCustomer();
        var accountNumber = openAccount(customer, POUND);
        userLogin(CUSTOMER);
        chooseCustomer(customer);
        findAccountWithAccountNumber(accountNumber, POUND);
    }

    @Test
    @DisplayName("Withdraw invalid amount of money from customer account")
    public void testWithdrawInvalidAmountFromAccount() {
        userLogin(CUSTOMER);
        chooseRandomCustomer();
        findAccountWithCurrency(DOLLAR);
        withdrawMoney();
    }

    @Step("Login")
    private void userLogin(String user) {
        switch (user) {
            case CUSTOMER -> loginPage.customerLoginButton.shouldBe(Condition.visible).click();
            case BANK_MANAGER -> loginPage.bankManagerLoginButton.shouldBe(Condition.visible).click();
            default -> loginPage.customerLoginButton.shouldBe(Condition.visible).click();
        }
    }

    @Step("Choose random customer")
    private void chooseRandomCustomer() {
        var countOfUsers = userSelectPage.userSelectDropDownList.shouldBe(Condition.visible).getOptions().size();
        var currentUser = 1 + (int) (Math.random() * (countOfUsers - 1));
        userSelectPage.userSelectDropDownList.selectOption(currentUser);
        userSelectPage.loginButton.shouldBe(Condition.visible).click();
    }

    @Step("Choose the customer {customer}")
    private void chooseCustomer(String customer) {
        userSelectPage.userSelectDropDownList.selectOption(customer);
        userSelectPage.loginButton.shouldBe(Condition.visible).click();
    }

    @Step("Find account with the special currency: {currency}")
    private void findAccountWithCurrency(String currency) {
        var userAccount = userAccountPage.accountSelectButton;
        var accountList = userAccount.getOptions();
        var currentCurrency = userAccountPage.getCurrency();
        if (!currentCurrency.equals(currency)) {
            for (int i = 0; i < accountList.size(); i++) {
                userAccount.selectOption(i);
            }
        }
    }

    @Step("Find the special account")
    private void findAccountWithAccountNumber(String accountNumber, String currency) {
        var userAccount = userAccountPage.accountSelectButton;
        var currentAccount = userAccountPage.accountNumber.getText();
        if (!currentAccount.equals(accountNumber)) {
            userAccount.selectOption(accountNumber);
        }
        var currentCurrency = userAccountPage.getCurrency();
        assertEquals(currency, currentCurrency);
    }

    @Step("Reset transaction history")
    private void resetTransactionsHistory() {
        userAccountPage.transactionsButton.click();
        if (transactionsPage.isTableNotEmpty()) {
            transactionsPage.resetButton.shouldBe(Condition.visible).click();
        }
        transactionsPage.backButton.shouldBe(Condition.visible).click();
    }

    @Step("Top up")
    private Integer topUp() {
        userAccountPage.deposit.shouldBe(Condition.visible).click();
        var amountToBeDeposited = userAccountPage.amountToBeDeposited;
        var amountOfMoney = 150 + (int) (Math.random() * 2500);
        amountToBeDeposited.shouldBe(Condition.visible).setValue(String.valueOf(amountOfMoney));
        userAccountPage.submitButton.shouldBe(Condition.visible).click();
        userAccountPage.depositSuccessfulMessage.shouldBe(Condition.visible);
        return amountOfMoney;
    }

    @Step("Check the balance")
    private void checkBalance(int amountOfMoney) {
        var balance = parseInt(userAccountPage.getBalance());
        assertTrue(balance >= amountOfMoney);
    }

    @Step("Check the transaction")
    private void checkTransaction(int amountOfMoney) {
        userAccountPage.transactionsButton.click();

        if (transactionsPage.isTableNotEmpty()) {
            Selenide.refresh();
        }

        var dateTimeOfTransactionString
            = transactionsPage.getDateTimeOfTransaction();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM d, yyyy h:mm:ss a");
        LocalDateTime dateTimeOfTransaction = LocalDateTime.parse(dateTimeOfTransactionString, formatter);
        LocalDate dateOfTransaction = dateTimeOfTransaction.toLocalDate();
        LocalDate currentDate = LocalDate.now();
        assertEquals(dateOfTransaction, currentDate);
        var amountOfDepositedMoney = parseInt(transactionsPage.getAmountOfMoney());
        assertEquals(amountOfDepositedMoney, amountOfMoney);
    }

    @Step("Withdraw money")
    private void withdrawMoney() {
        var balance = userAccountPage.getBalance();
        userAccountPage.withdrawButton.shouldBe(Condition.visible).click();
        userAccountPage.amountToBeWithdrawn.shouldBe(Condition.editable).sendKeys(balance + 1);
        userAccountPage.submitButton.shouldBe(Condition.visible).click();
        assertEquals(userAccountPage.submitWithdrawalRequestErrorMessage.shouldBe(Condition.visible).getText(),
                ERROR_MESSAGE);
    }

    @Step("Create a new customer")
    private String createNewCustomer() {
        bankManagerPage.addCustomerButton.shouldBe(Condition.visible).click();
        var firstname = getFirstName();
        var lastname = getLastName();
        bankManagerPage.firstName.shouldBe(Condition.editable).sendKeys(firstname);
        bankManagerPage.lastName.shouldBe(Condition.editable).sendKeys(lastname);
        bankManagerPage.postCode.shouldBe(Condition.editable).sendKeys(getPostcode());
        bankManagerPage.submitButton.shouldBe(Condition.visible).click();
        assertTrue(handlingPopUps().contains(CUSTOMER_ADDED_MESSAGE));
        return firstname + " " + lastname;
    }

    @Step("Open the {currency} account of {customer}")
    private String openAccount(String customer, String currency) {
        bankManagerPage.openAccount.shouldBe(Condition.visible).click();
        bankManagerPage.userSelect.shouldBe(Condition.visible).selectOption(customer);
        bankManagerPage.currency.shouldBe(Condition.visible).selectOption(currency);
        bankManagerPage.submitButton.shouldBe(Condition.visible).click();
        var accountNumber = getAccountNumber();
        loginPage.homeButton.shouldBe(Condition.visible).click();

        return accountNumber;
    }

    private String handlingPopUps() {
        Alert alert = switchTo().alert();
        return alert.getText();
    }

    private String getAccountNumber() {
        var allertMessage = handlingPopUps();
        String[] splitMessage = allertMessage.split(":");
        return splitMessage[1];
    }

}
